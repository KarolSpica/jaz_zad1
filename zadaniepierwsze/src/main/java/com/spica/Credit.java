package com.spica;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Servlet implementation class Credit
 */
@WebServlet("/Credit")
public class Credit extends HttpServlet {
	private static final long serialVersionUID = 1L;      

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//walidacja
		if(request.getParameter("kwota").isEmpty()||request.getParameter("iloscRat").isEmpty()||request.getParameter("oprocentowanie").isEmpty()||request.getParameter("oplataStala").isEmpty()||request.getParameter("rodzajRaty").isEmpty()||!isNumeric(request.getParameter("kwota"))||!isNumeric(request.getParameter("iloscRat"))||!isNumeric(request.getParameter("oprocentowanie"))||!isNumeric(request.getParameter("oplataStala"))) {
			response.getWriter().println("Puste pole lub zle dane");
			
		} else {
	   //tworzenie obiektu client
	    Client client = new Client(Double.parseDouble(request.getParameter("kwota")),Double.parseDouble(request.getParameter("iloscRat")),Double.parseDouble(request.getParameter("oprocentowanie")),Double.parseDouble(request.getParameter("oplataStala")),request.getParameter("rodzajRaty"));
		
		    //obliczanie rat i tworzenie tabeli   
			
		    if(client.getRodzajRat().equalsIgnoreCase("Malejaca")) {
		    	response.getWriter().println("<table style="+'"'+"border: 1px solid black; margin-top:10px; margin-left:10px;"+'"'+">");
				response.getWriter().println("<tr><th>Numer raty</th><th>Rata odsetkowa</th><th>Rata kapitalow</th><th>Oplata stala</th><th>Rata calkowita</th>");
		    	for(int i=0; i < client.getIloscRat(); i++) {
		    		double rataOdsetkowa = ((((client.getKwota()-i*(client.getKwota())/client.getIloscRat()))*(client.getOprocentowanie()/100))/12);
		    		response.getWriter().println("<tr><th>"+(i+1)+"</th>"+"<th>"+String.format("%.2f", rataOdsetkowa)+"</th><th>"+String.format("%.2f", (client.getKwota()/client.getIloscRat()))+"</th><th>" +String.format("%.2f",client.getOplataStala()/client.getIloscRat())+"</th><th>"+String.format("%.2f", (rataOdsetkowa+(client.getKwota()/client.getIloscRat())+(client.getOplataStala()/client.getIloscRat())))+"</th>");
		    
		    	}
		    	response.getWriter().println("</table>");
		    } else {
		    	double wspolczynnikProcentowy = 1+((0.0833333)*(client.getOprocentowanie()/100));
		    	response.getWriter().println("Rata kredytu stalego wynosi: "+String.format("%.2f",((client.getKwota()*(Math.pow(wspolczynnikProcentowy,client.getIloscRat()))*(wspolczynnikProcentowy-1))/((Math.pow(wspolczynnikProcentowy,client.getIloscRat()))-1)+client.getOplataStala()/client.getIloscRat())));
		    }
		}
	}
	
	public static boolean isNumeric(String str) {
		try {
			@SuppressWarnings("unused")
			double d = Double.parseDouble(str);
		}
		catch(NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	
}
