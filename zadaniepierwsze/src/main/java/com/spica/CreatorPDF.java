package com.spica;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;



@WebServlet("/CreatorPDF")
public class CreatorPDF extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void service(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		 
		//walidacja
		if(request.getParameter("kwota").isEmpty()||request.getParameter("iloscRat").isEmpty()||request.getParameter("oprocentowanie").isEmpty()||request.getParameter("oplataStala").isEmpty()||request.getParameter("rodzajRaty").isEmpty()||!isNumeric(request.getParameter("kwota"))||!isNumeric(request.getParameter("iloscRat"))||!isNumeric(request.getParameter("oprocentowanie"))||!isNumeric(request.getParameter("oplataStala"))) {
			response.getWriter().println("Puste pole lub zle dane");
					
		} else {
		Client client = new Client(Double.parseDouble(request.getParameter("kwota")),Double.parseDouble(request.getParameter("iloscRat")),Double.parseDouble(request.getParameter("oprocentowanie")),Double.parseDouble(request.getParameter("oplataStala")),request.getParameter("rodzajRaty"));	
		if(client.getRodzajRat().equalsIgnoreCase("Malejaca")) {
		try {
			 
	            Document document = new Document();
	         
	            ByteArrayOutputStream baos = new ByteArrayOutputStream();
	            PdfWriter.getInstance(document, baos);
	           
	            document.open();
	        
	            
	            document.add((createTable(client)));
	           
	            document.close();
	 
	            
	            response.setHeader("Expires", "0");
	            response.setHeader("Cache-Control",
	                "must-revalidate, post-check=0, pre-check=0");
	            response.setHeader("Pragma", "public");
	           
	            response.setContentType("application/pdf");
	            
	            response.setContentLength(baos.size());
	          
	            OutputStream os = response.getOutputStream();
	            baos.writeTo(os);
	            os.flush();
	            os.close();
	        }
	        catch(DocumentException e) {
	            throw new IOException(e.getMessage());
	        }
		} else {
			response.getWriter().println("Nie zdazylem ;)");
		}
		}
	}
	public static PdfPTable createTable(Client client){
	
        PdfPTable table = new PdfPTable(5);
        PdfPCell cell;
        String [] data = {
        		"Numer raty","Rata odsetkowa","Rata kapitalowa","Oplata stala","Rata calkowita"
        };
        for(int i = 0; i < data.length; i++) {
        	cell = new PdfPCell(new Phrase(data[i]));
        	cell.setColspan(1);
        	table.addCell(cell);
        }
    
        for(int i=0; i < client.getIloscRat(); i++) {
        	double rataOdsetkowa = ((((client.getKwota()-i*(client.getKwota())/client.getIloscRat()))*(client.getOprocentowanie()/100))/12);
        	cell = new PdfPCell(new Phrase(Integer.toString(i+1)));
        	cell.setColspan(1);
        	table.addCell(cell);
        	
        	cell = new PdfPCell(new Phrase(String.format("%.2f", rataOdsetkowa)));
        	cell.setColspan(1);
        	table.addCell(cell);
        	
        	cell = new PdfPCell(new Phrase(String.format("%.2f", (client.getKwota()/client.getIloscRat()))));
        	cell.setColspan(1);
        	table.addCell(cell);
        	
        	cell = new PdfPCell(new Phrase(String.format("%.2f",(client.getOplataStala()/client.getIloscRat()))));
        	cell.setColspan(1);
        	table.addCell(cell);
        	
        	cell = new PdfPCell(new Phrase(String.format("%.2f", (rataOdsetkowa+(client.getKwota()/client.getIloscRat())+(client.getOplataStala()/client.getIloscRat())))));
        	cell.setColspan(1);
        	table.addCell(cell);
        }
		return table;
	}
	public static boolean isNumeric(String str) {
		try {
			@SuppressWarnings("unused")
			double d = Double.parseDouble(str);
		}
		catch(NumberFormatException nfe) {
			return false;
		}
		return true;
	}
}
