package com.spica;

public class Client {
	@Override
	public String toString() {
		return "Client [kwota=" + kwota + ", iloscRat=" + iloscRat + ", oprocentowanie=" + oprocentowanie
				+ ", oplataStala=" + oplataStala + ", rodzajRat=" + rodzajRat + "]";
	}
	private double kwota;
	private double iloscRat;
	private double oprocentowanie;
	private double oplataStala;
	private String rodzajRat;
	
	public Client(double kwota, double iloscRat, double oprocentowanie, double oplataStala, String rodzajRat) {
		super();
		this.kwota = kwota;
		this.iloscRat = iloscRat;
		this.oprocentowanie = oprocentowanie;
		this.oplataStala = oplataStala;
		this.rodzajRat = rodzajRat;
	}
	public double getKwota() {
		return kwota;
	}
	public void setKwota(double kwota) {
		this.kwota = kwota;
	}
	public double getIloscRat() {
		return iloscRat;
	}
	public void setIloscRat(double iloscRat) {
		this.iloscRat = iloscRat;
	}
	public double getOprocentowanie() {
		return oprocentowanie;
	}
	public void setOprocentowanie(double oprocentowanie) {
		this.oprocentowanie = oprocentowanie;
	}
	public double getOplataStala() {
		return oplataStala;
	}
	public void setOplataStala(double oplataStala) {
		this.oplataStala = oplataStala;
	}
	public String getRodzajRat() {
		return rodzajRat;
	}
	public void setRodzajRat(String rodzajRat) {
		this.rodzajRat = rodzajRat;
	}
}
